---
template: blog-post
title: Description of basic skate tricks? Learn skateboarding
slug: /description-of-basic-skateboard-tricks
date: 2021-09-23 03:58
description: Any serious skateboarder will tell you that ollie is the most basic
  of all skateboarding tricks. As a rule, this is the very first trick you start
  to learn by standing on the board.
featuredImage: /assets/61lcbtgsdul._ac_sl1000_.jpg
---
Basic skateboarding tricks with which a beginner rider should start learning. 

**Legend:**

**BS** - Backside.\
**FS** - Frontside.\
**Tail** is the tail of the deck.

## Ollie

A basic skateboarding trick in which the body and board are lifted into the air without using your hands. For better ollie you need [best skateboards for tricks](https://skatetownguide.com/best-skateboard-for-tricks/).

Any serious skateboarder will tell you that ollie is the most basic of all skateboarding tricks. As a rule, this is the very first trick you start to learn by standing on the board.

1. Place your back foot on the tail of the board and your front foot approximately in the middle of the board or slightly closer to the front bolts.
2. Begin to bend your knees and squat. At the same time, click on the tail with your back foot and immediately begin to hold the nose of the board with your front foot in an outward motion.
3. Press your knees to your chest and land on the ground. Try to do this with both legs at the same time and place your legs closer to the harnesses.

## Fakie Ollie

**This trick is done in exactly the same way as nollie, only in a switch rack. This trick is done in exactly the same way as nollie, only in a switch rack.**

1. You are standing in a switch rack. The front foot is on the nose of the board. The back is in the area of ​​the bolts closer to the center of the board.
2. Click with your front foot on the nose of the board and slide your back foot out along the skin while holding the board.
3. Bend your knees and land in fakes.

## Nollie

**The trick is similar to the *Ollie* , but to take off, the rider flicks on the nose of the board, that is, from an uncomfortable foot, moving forward in a normal stance.**

This trick is very similar to the **ollie** , except that you are not clicking on the tail of the board, but on the nose of the board, moving forward in your usual stance, that is, clicking from an uncomfortable foot.

1. The front foot is on the nose of the board. The back is in the area of ​​the bolts closer to the center of the board.
2. Click with your front foot on the nose of the board and bring your back foot out to the skin, thereby holding the board.
3. Bend your knees and land.

### Switch Ollie

**The trick looks like *Ollie* , but is done in a *switch* rack.**

This one-on-one trick looks like an **ollie** , except that it is done in a **switch** rack.

1. Stand in the switch rack. Place your back foot on the tail of the board and your front foot approximately in the middle of the board or slightly closer to the front bolts.
2. Begin to bend your knees and squat. At the same time, click on the tail with your back foot and immediately begin to hold the nose of the board with your front foot in an outward motion.
3. Press your knees to your chest and land on the ground. Try to do this with both legs at the same time and place your legs closer to the harnesses.
4. You leave while staying in the switch rack.

### FS 180

**A 180 ° turn with your face in the direction of your skate.**

1. The front foot is at the bolts, so that the toes are barely hanging over the edge of the board. The back foot should be on the inside of the tail. The heel hangs down completely.
2. Sit down a bit and prepare your shoulders for a rotation behind your back.
3. Click on the tail and start rolling with your shoulders, your legs will follow them.
4. Make a 180-degree turn behind your back, land and go into fakes.

### BS 180

**A 180 ° turn with your back in the direction of travel of your skate.**

1. The front foot is at the bolts, so that the toes are barely hanging over the edge of the board. The back foot should be on the inside of the tail. The heel hangs down completely.
2. Sit down slightly and prepare your shoulders for a forward-facing rotation.
3. Click on the tail and start rolling with your shoulders, your legs will follow them.
4. Make a 180-degree turn with your back in the direction of travel, land and go into fakes.

### FS Half Cab

**You snap out of fakie and spin 180 ° backwards with the board.**

This trick is the **Fakie Frontside 180** . But it has the name **Half Cab** derived from **Full Cab** , that is, **Backside Caballerial'a** . **Flicking** out **fakie** your body and the board doing **180** degrees behind your back.

1. Legs like fakie ollie.
2. Roll your shoulders in the opposite direction to the rotation, which helps the rotation and flick starting to rotate.
3. The board rotates 180 degrees with you. When the spin ends, land.

 

### BS Half Cab

**You snap out of fakie and rotate 180 ° face forward with the board.**

This trick is **Fakie Backside 180** . But it has the name **Half Cab** derived from **Full Cab** , that is, **Backside Caballerial'a** . **Flicking** out **fakie** your body and the board makes **180** degrees across your face forward.

1. Legs like fakie ollie.
2. Bring your shoulders in the opposite direction to the rotation, that is, behind your back, which helps the rotation and click starting to rotate.
3. The board rotates 180 degrees with you. When the spin ends, land.

 

### Nollie FS 180

**This is a trick in which by flicking out of the *nollie* your body and the board *180 °*  forward in the direction of travel.**

1. Legs like nollie. Front foot on the nose. The back is closer to the center of the bolts, the toe is on the board, the heel hangs down.
2. Roll your shoulders in the opposite direction to the rotation, which helps the rotation and flick starting to rotate.
3. The board rotates 180 degrees with you. When the spin ends, land.

 

### Nollie BS 180

**This is a trick in which flicking out of the *nollie* your body and the board does *180 °*  behind your back.**

1. Legs like nollie. Front foot on the nose. The back is closer to the center of the bolts, the toe hangs off the board.
2. Roll your shoulders in the opposite direction to the rotation, which helps the rotation and flick starting to rotate.
3. The board rotates 180 degrees with you. When the spin ends, land.

 

### Switch FS 180

**A *180 °* turn to face the direction of your skate, which you do by clicking from the *switch* rack.**

1. You get into the switch rack. The front foot is at the bolts, so that the toes are barely hanging over the edge of the board. The back foot should be on the inside of the tail. The heel hangs down completely.
2. Sit down a little and prepare your shoulders for a rotation behind your back, twisting them in the opposite direction from the rotation.
3. Click on the tail and start rolling with your shoulders, your legs will follow them.
4. Make a 180-degree turn to face the direction of travel, land and drive away in your normal stance.

### Switch BS 180

**A *180 °* turn with your back in the direction of travel of your skate, which you do by clicking from the *switch* rack.**

1. You get into the switch rack. The front foot is at the bolts, so that the toes are barely hanging over the edge of the board. The back foot should be on the inside of the tail. The heel hangs down completely.
2. Sit down slightly and prepare your shoulders for a forward-facing rotation.
3. Click on the tail and start rolling with your shoulders, your legs will follow them.
4. Make a 180-degree turn with your back in the direction of travel, land and drive away in your normal stance.

### FS 360

**Full rotation around its axis *360 °* forward facing in relation to movement.**

1. The back, snap leg is on the tail at a 45 degree angle. The front leg is between the middle of the board and the bolts. The sock hangs off the board. You seem to completely wrap your foot around the board. And heel and toe.
2. Start your shoulders in the opposite direction from rotation, that is, behind your back. Squat and click.
3. You should rotate pretty quickly due to the initial movement of your shoulders while holding the board with your front leg. Do a full 360-degree turn behind your back and land.

## BS 360

**Backward rotation *360 °* .**

1. Legs as on bs 180. Hind leg on tail. Front at a slight angle in the area of ​​the nose bolts.
2. Get ready for a 360-degree rotation, bring your shoulders back, it is due to the rotation of the torso that you and the board will rotate.
3. Click and immediately start spinning under the backside.
4. After landing, continue rotating your body, thereby tightening 360.

 

## FS Caballerial

**This is a frontside *360 °* rotation from *fakie* . The trick is named after the legendary skater *Steve Caballero* .**

 

### BS Caballerial

**360 ° rotation facing forward in the direction of travel from the *fakie* stance. In fact, this is *Fakie Backside 360* .**

This trick is named after the legendary skater **Steve Caballero** , who was the first to do this trick back in the 70s of the last century. Its essence is that you rotate **360** degrees facing forward in the direction of travel from the **fakie** rack. In fact, this is **Fakie Backside 360** .

1. Legs like fakie ollie. Prepare your body for the trick by throwing your shoulders in the opposite direction from the rotation, that is, your shoulders behind your back.
2. Click and start curling your shoulders under the backside.
3. Do a full 360 and land. Leave at the switch rack.

 Nollie FS 360

**When clicking from *Nollie* , you *rotate 360 °* face forward with the board.**

### Nollie BS 360

***Clicking* from *Nollie takes* you *360 °* spinning backwards with the board.**

### Switch FS 360

**A *360 °* rotation with your face in the direction of your skate, which you do by flipping out of the *switch* rack.**

### Switch BS 360

**A *360 °* rotation with your back in the direction of travel of your skate, which you do by flipping out of the *switch* rack.**

### Ollie North

***Ollie* in which you take your front foot off the board.**

**Source: [skatetownguide](https://skatetownguide.com/)**